package com.cyril.catererservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cyril.catererservice.controller.CatererController;
import com.cyril.catererservice.service.CatererService;

@RunWith(SpringRunner.class)
@WebMvcTest(CatererController.class)
public class PayloadValidationMvcTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	CatererService catererService;

	@Test
	public void validateContactFields() throws Exception {
		String requestJson = "{\n" + "	\"name\" : \"cyril2\",\n" + "	\"location\" : {\n"
				+ "		\"cityName\" : \"ernakulam\",\n" + "		\"street\" : \"mattoor2\",\n"
				+ "		\"postal\" : 12342342\n" + "	},\n" + "	\"capacity\" : {\n" + "		\"min\" : 102,\n"
				+ "		\"max\" : 202\n" + "	},\n" + "	\"contact\" : {\n" + "		\"phone\" : \"qwer\",\n"
				+ "		\"mobile\" : \"+9897 2352767652\",\n" + "		\"email\" : \"avc@gmail.com2\"\n" + "	}\n"
				+ "}";
		String message = this.mockMvc
				.perform(MockMvcRequestBuilders.post("/caterers").contentType(MediaType.APPLICATION_JSON_VALUE)
						.accept(MediaType.APPLICATION_JSON_VALUE).content(requestJson))
				.andExpect(status().isBadRequest())
//				.andReturn().getResolvedException().getMessage();
				.andReturn().getResponse().getContentAsString();
		assertEquals(
				"{\"errors\":[" + "\"caterer.contact.email=avc@gmail.com2. Reason for rejection:Invalid email id\","
						+ "\"caterer.contact.mobile=+9897 2352767652. Reason for rejection:Invalid phone number\","
						+ "\"caterer.contact.phone=qwer. Reason for rejection:Invalid phone number\"]}",
				message);

	}

	@Test
	public void validateCapacityMinMaxFields() throws Exception {
		String requestJson = "{\n" + "	\"name\" : \"cyril2\",\n" + "	\"location\" : {\n"
				+ "		\"cityName\" : \"ernakulam\",\n" + "		\"street\" : \"mattoor2\",\n"
				+ "		\"postal\" : 12342342\n" + "	},\n" + "	\"capacity\" : {\n" + "		\"min\" : 302,\n"
				+ "		\"max\" : 202\n" + "	},\n" + "	\"contact\" : {\n"
				+ "		\"phone\" : \"(123)123 1231\",\n" + "		\"mobile\" : \"123-123 1231\",\n"
				+ "		\"email\" : \"avc@gmail.com\"\n" + "	}\n" + "}";
		String message = this.mockMvc
				.perform(MockMvcRequestBuilders.post("/caterers").contentType(MediaType.APPLICATION_JSON_VALUE)
						.accept(MediaType.APPLICATION_JSON_VALUE).content(requestJson))
				.andExpect(status().isBadRequest())
//				.andReturn().getResolvedException().getMessage();
				.andReturn().getResponse().getContentAsString();
		assertEquals("{\"errors\":[\"caterer.capacity=Capacity(min=302, max=202). "
				+ "Reason for rejection:Min value should be <= Max value!!\"]}", message);

	}

	@Test
	public void validateLocationCityNameField() throws Exception {
		String requestJson = "{\n" + "	\"name\" : \"cyril2\",\n" + "	\"location\" : {\n"
				+ "		\"cityName\" : \"ernakulam2\",\n" + "		\"street\" : \"mattoor2\",\n"
				+ "		\"postal\" : 12342342\n" + "	},\n" + "	\"capacity\" : {\n" + "		\"min\" : 102,\n"
				+ "		\"max\" : 202\n" + "	},\n" + "	\"contact\" : {\n"
				+ "		\"phone\" : \"(123)123 1231\",\n" + "		\"mobile\" : \"123-123 1231\",\n"
				+ "		\"email\" : \"avc@gmail.com\"\n" + "	}\n" + "}";
		String message = this.mockMvc
				.perform(MockMvcRequestBuilders.post("/caterers").contentType(MediaType.APPLICATION_JSON_VALUE)
						.accept(MediaType.APPLICATION_JSON_VALUE).content(requestJson))
				.andExpect(status().isBadRequest())
//				.andReturn().getResolvedException().getMessage();
				.andReturn().getResponse().getContentAsString();
		assertEquals("{\"errors\":[\"caterer.location.cityName=ernakulam2. "
				+ "Reason for rejection:Contains digits and special charaxters\"]}", message);

	}
}