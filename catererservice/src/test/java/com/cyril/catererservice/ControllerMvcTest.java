package com.cyril.catererservice;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cyril.catererservice.dao.CatererRepository;
import com.cyril.catererservice.dto.CatererDto;
import com.cyril.catererservice.model.Capacity;
import com.cyril.catererservice.model.Caterer;
import com.cyril.catererservice.model.Contact;
import com.cyril.catererservice.model.Location;
import com.cyril.catererservice.service.KafkaConsumer;
import com.cyril.catererservice.service.KafkaProducer;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureDataMongo
@EmbeddedKafka(partitions = 1, brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" })

public class ControllerMvcTest {
	private String topic = "caterer-topic";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private CatererRepository catererRepository;

	@Autowired
	private KafkaConsumer consumer;

	@Autowired
	private KafkaProducer producer;

	@Test
	@Order(1)
	public void test1CreateCaterer() throws Exception {

		// clean db
		catererRepository.deleteAll();

		// verify db has 0 elements
		List<CatererDto> list = catererRepository.findByName("asd");
		assertEquals(0, list.size());

		Location loc = new Location("Kochi", "10th street", 1234);
		Capacity cap = new Capacity(12, 60);
		Contact con = new Contact("8884593719", "8884593719", "asdc@qwe.com");
		Caterer caterer = new Caterer(null, "asd", loc, cap, con);

		mockMvc.perform(MockMvcRequestBuilders.post("/caterers").contentType("application/json")
				.content(objectMapper.writeValueAsString(caterer))).andExpect(status().isCreated());

		// verify db has 0 elements
		List<CatererDto> list1 = catererRepository.findByName("asd");
		assertEquals(1, list1.size());
		assertEquals("asd", list1.get(0).getName());
	}

	@Test
	@Order(2)
	public void test2GETApis() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/caterers")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("asd")));

		List<CatererDto> list1 = catererRepository.findByName("asd");
		String id = list1.get(0).getId();

		mockMvc.perform(MockMvcRequestBuilders.get("/caterers/" + id)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("asd")));


		mockMvc.perform(MockMvcRequestBuilders.get("/caterers/findbyname").param("name", "asd")).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(containsString("asd")));

		mockMvc.perform(MockMvcRequestBuilders.get("/caterers/findbycityname").param("cityName", "Kochi")
				.param("pageNum", "0").param("pageSize", "1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("asd")));

	}

	@Test
	@Order(3)
	public void test3KafkaReceivedEntry() throws Exception {
		consumer.getLatch().await(10000, TimeUnit.MILLISECONDS);

		assertThat(consumer.getLatch().getCount(), equalTo(0L));
		assertThat(consumer.getPayload(),
				containsString("name=asd, location=Location(cityName=Kochi, street=10th street, postal=1234), "
						+ "capacity=Capacity(min=12, max=60), contact=Contact(phone=8884593719, mobile=8884593719, email=asdc@qwe.com)))"));
	}

}