package com.cyril.catererservice.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cyril.catererservice.exception.RecordNotFoundException;
import com.cyril.catererservice.exception.ServerInternalException;
import com.cyril.catererservice.model.Caterer;
import com.cyril.catererservice.service.CatererService;
import com.cyril.catererservice.service.KafkaProducer;

@RestController
public class CatererController {

	@Autowired
	CatererService catererService;
	
	@Autowired
	KafkaProducer kafkaProducer;

	@GetMapping("caterers")
	public @ResponseBody ResponseEntity<List<Caterer>> all() {
		List<Caterer> all = catererService.all();
		for (Caterer caterer : all) {

			caterer.add(linkTo(methodOn(CatererController.class).getById(caterer.getId())).withSelfRel());
			caterer.add(linkTo(methodOn(CatererController.class).getByName(caterer.getName())).withSelfRel());
			caterer.add(
					linkTo(methodOn(CatererController.class).getByCityName(caterer.getLocation().getCityName(), 1, 1))
							.withSelfRel());

		}
		return new ResponseEntity<>(all, HttpStatus.OK);
	}

	@GetMapping("caterers/{id}")
	public ResponseEntity<Caterer> getById(@PathVariable String id) {
		Optional<Caterer> caterer = catererService.getById(id);
		if (caterer.isPresent()) {
			Caterer caterer2 = caterer.get();

			caterer2.add(linkTo(methodOn(CatererController.class).all()).withSelfRel());
			caterer2.add(linkTo(methodOn(CatererController.class).getByName(caterer2.getName())).withSelfRel());
			caterer2.add(
					linkTo(methodOn(CatererController.class).getByCityName(caterer2.getLocation().getCityName(), 1, 1))
							.withSelfRel());

			return new ResponseEntity<>(caterer2, HttpStatus.OK);
		} else {
			throw new RecordNotFoundException("Invalid caterer id : " + id);
		}
	}

	@GetMapping("caterers/findbyname")
	public ResponseEntity<List<Caterer>> getByName(@RequestParam String name) {
		List<Caterer> caterers = catererService.getByName(name);
		for (Caterer caterer2 : caterers) {

			caterer2.add(linkTo(methodOn(CatererController.class).all()).withSelfRel());
			caterer2.add(linkTo(methodOn(CatererController.class).getById(caterer2.getId())).withSelfRel());
			caterer2.add(
					linkTo(methodOn(CatererController.class).getByCityName(caterer2.getLocation().getCityName(), 1, 1))
							.withSelfRel());
		}

		return new ResponseEntity<>(caterers, HttpStatus.OK);
	}

	@GetMapping("caterers/findbycityname")
	public ResponseEntity<List<Caterer>> getByCityName(@RequestParam String cityName, @RequestParam Integer pageNum,
			@RequestParam Integer pageSize) {
		List<Caterer> caterers = catererService.getByCityName(cityName, pageNum, pageSize);
		for (Caterer caterer2 : caterers) {

			caterer2.add(linkTo(methodOn(CatererController.class).all()).withSelfRel());
			caterer2.add(linkTo(methodOn(CatererController.class).getById(caterer2.getId())).withSelfRel());
			caterer2.add(linkTo(methodOn(CatererController.class).getByName(caterer2.getName())).withSelfRel());

		}

		return new ResponseEntity<>(caterers, HttpStatus.OK);
	}

	@PostMapping(path = "caterers", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Caterer> create(@RequestBody @Valid Caterer caterer) {
		Caterer caterer2 = catererService.create(caterer);
		if (caterer2 == null) {
			throw new ServerInternalException("Failed to create a new caterer");
		}
		
		// Submit the saved caterer for kafka-publish
		kafkaProducer.submit(caterer2);
		
		caterer2.add(linkTo(methodOn(CatererController.class).all()).withSelfRel());
		caterer2.add(linkTo(methodOn(CatererController.class).getById(caterer2.getId())).withSelfRel());
		caterer2.add(linkTo(methodOn(CatererController.class).getByName(caterer2.getName())).withSelfRel());
		caterer2.add(
				linkTo(methodOn(CatererController.class).getByCityName(caterer2.getLocation().getCityName(), 1, 1))
						.withSelfRel());

		return new ResponseEntity<>(caterer2, HttpStatus.CREATED);
	}

	@PutMapping(path = "caterers", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Caterer> save(@RequestBody @Valid Caterer caterer) {
		Caterer caterer2 = catererService.create(caterer);
		if (caterer2 == null) {
			throw new ServerInternalException("Failed to create a new caterer");
		}
		
		caterer2.add(linkTo(methodOn(CatererController.class).all()).withSelfRel());
		caterer2.add(linkTo(methodOn(CatererController.class).getById(caterer2.getId())).withSelfRel());
		caterer2.add(linkTo(methodOn(CatererController.class).getByName(caterer2.getName())).withSelfRel());
		caterer2.add(
				linkTo(methodOn(CatererController.class).getByCityName(caterer2.getLocation().getCityName(), 1, 1))
						.withSelfRel());

		return new ResponseEntity<>(caterer2, HttpStatus.CREATED);
	}

}
