package com.cyril.catererservice.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.cyril.catererservice.dao.CatererRepository;
import com.cyril.catererservice.dto.CapacityDto;
import com.cyril.catererservice.dto.CatererDto;
import com.cyril.catererservice.dto.ContactDto;
import com.cyril.catererservice.dto.LocationDto;
import com.cyril.catererservice.model.Capacity;
import com.cyril.catererservice.model.Caterer;
import com.cyril.catererservice.model.Contact;
import com.cyril.catererservice.model.Location;

@Service
public class CatererService {

	
	@Autowired
	CatererRepository catererRepository;

	public Caterer create(Caterer caterer) {
		CatererDto catererDto = catererRepository.save(mapToDto(caterer));
		
		return mapFromDto(catererDto);
	}

	public List<Caterer> all() {
		List<CatererDto> findAll = catererRepository.findAll();
		List<Caterer> list = findAll.stream().map(this::mapFromDto).collect(Collectors.toList());
		return list;
	}

	public Optional<Caterer> getById(String id) {
		Optional<CatererDto> catererDto = catererRepository.findById(id);
		if (catererDto.isPresent())
			return Optional.of(mapFromDto(catererDto.get()));
		else
			return Optional.empty();
	}

	public List<Caterer> getByName(String name) {
		List<CatererDto> catererDtos = catererRepository.findByName(name);
		List<Caterer> list = catererDtos.stream().map(this::mapFromDto).collect(Collectors.toList());
		return list;
	}

	private CatererDto mapToDto(Caterer caterer) {
		CatererDto catererDto = new CatererDto();
		catererDto.setId(caterer.getId());
		catererDto.setName(caterer.getName());
		catererDto.setLocation(mapLocationToDto(caterer.getLocation()));
		catererDto.setCapacity(mapToCapacityDto(caterer.getCapacity()));
		catererDto.setContact(mapContactToDto(caterer.getContact()));
		return catererDto;
	}

	private LocationDto mapLocationToDto(Location location) {
		LocationDto location2 = new LocationDto();
		location2.setCityName(location.getCityName());
		location2.setPostal(location.getPostal());
		location2.setStreet(location.getStreet());
		return location2;
	}

	private ContactDto mapContactToDto(Contact contact) {
		ContactDto contact2 = new ContactDto();
		contact2.setPhone(contact.getPhone());
		contact2.setMobile(contact.getMobile());
		contact2.setEmail(contact.getEmail());

		return contact2;
	}

	private CapacityDto mapToCapacityDto(Capacity capacity) {
		CapacityDto capacity2 = new CapacityDto();
		capacity2.setMax(capacity.getMax());
		capacity2.setMin(capacity.getMin());

		return capacity2;
	}

	private Caterer mapFromDto(CatererDto catererDto) {
		Caterer caterer = new Caterer();
		caterer.setId(catererDto.getId());
		caterer.setName(catererDto.getName());
		caterer.setLocation(mapLocationFromDto(catererDto.getLocation()));
		caterer.setCapacity(mapCapacityFromDto(catererDto.getCapacity()));
		caterer.setContact(mapContactFromDto(catererDto.getContact()));

		return caterer;
	}

	private Location mapLocationFromDto(LocationDto location) {
		Location location2 = new Location();
		location2.setCityName(location.getCityName());
		location2.setPostal(location.getPostal());
		location2.setStreet(location.getStreet());
		return location2;
	}

	private Contact mapContactFromDto(ContactDto contact) {
		Contact contact2 = new Contact();
		contact2.setPhone(contact.getPhone());
		contact2.setMobile(contact.getMobile());
		contact2.setEmail(contact.getEmail());

		return contact2;
	}

	private Capacity mapCapacityFromDto(CapacityDto capacity) {
		Capacity capacity2 = new Capacity();
		capacity2.setMax(capacity.getMax());
		capacity2.setMin(capacity.getMin());

		return capacity2;
	}

	public List<Caterer> getByCityName(String cityName, Integer pageNum, Integer pageSize) {
		PageRequest pageRequest = PageRequest.of(pageNum, pageSize, Sort.by("name"));
		
		Page<CatererDto> catererDtos = catererRepository.findByCityName(cityName , pageRequest);
		return catererDtos.stream().map(this::mapFromDto).collect(Collectors.toList());
	}

}
