package com.cyril.catererservice.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.kafka.common.serialization.Serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.cyril.catererservice.model.Caterer;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaProducer {

	public static class CatererSerializer implements Serializer<Caterer> {

		@Override
		public byte[] serialize(String topic, Caterer data) {

			return data.toString().getBytes();
		}

	}

	private static final String TOPIC = "caterer-topic";

	@Autowired
	private KafkaTemplate<String, Caterer> kafkaTemplate;

	@Getter
	private BlockingQueue<Caterer> queue = new ArrayBlockingQueue<>(1000);

	private Thread thread;

	@PostConstruct
	void startKafkaPublisher() {

		thread = new Thread(new Runnable() {
			public void run() {
				while (!thread.isInterrupted()) {
					Caterer caterer = queue.poll();
					if (caterer != null) {
						kafkaTemplate.send(TOPIC, caterer.getId(), caterer);
					}

				}

			}
		});
		thread.start();

	}

	@PreDestroy
	void stopKafkaPublisher() {
		thread.interrupt();
	}

	public void submit(Caterer caterer2) {
		boolean offer = queue.offer(caterer2);
		if (offer) {
			log.info("Submitted caterer: {} for kafka-publish", caterer2.getId());
		} else {
			log.error("Failed to submit caterer: {} for kafka-publish", caterer2.getId());
		}

	}

}
