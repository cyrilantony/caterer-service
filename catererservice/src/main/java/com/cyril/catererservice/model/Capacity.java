package com.cyril.catererservice.model;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

import com.cyril.catererservice.validator.MinMaxConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Valid
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@MinMaxConstraint.List({
		@MinMaxConstraint(minfield = "min", maxfield = "max", message = "Min value should be <= Max value!!") })
public class Capacity {

	@Positive
	Integer min;
	@Positive
	Integer max;

}
