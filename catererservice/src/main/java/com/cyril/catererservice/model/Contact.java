package com.cyril.catererservice.model;

import javax.validation.Valid;

import com.cyril.catererservice.validator.ContactNumberConstraint;
import com.cyril.catererservice.validator.EmailConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Valid
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Contact {

	@ContactNumberConstraint(optional = true)
	String phone;

	@ContactNumberConstraint
	String mobile;

	@EmailConstraint
	String email;

}
