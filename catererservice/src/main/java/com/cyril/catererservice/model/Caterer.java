package com.cyril.catererservice.model;

import javax.validation.Valid;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Valid
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Caterer extends RepresentationModel<Caterer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -994318829858546818L;

	private String id;
	private String name;
	@Valid
	private Location location;
	@Valid
	private Capacity capacity;

	@Valid
	private Contact contact;

}
