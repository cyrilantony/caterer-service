package com.cyril.catererservice.model;

import javax.validation.Valid;

import com.cyril.catererservice.validator.AlphabetsOnlyConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Valid
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Location {

	@AlphabetsOnlyConstraint
	private String cityName;
	private String street;
	private Integer postal;

}
