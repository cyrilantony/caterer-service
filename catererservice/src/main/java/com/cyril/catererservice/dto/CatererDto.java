package com.cyril.catererservice.dto;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document(collection = "caterer")
@Getter @Setter @NoArgsConstructor @ToString
public class CatererDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -994318829858546818L;
	
	private  @Id String id;
	private String name;
	private LocationDto location;
	private CapacityDto capacity;
	private ContactDto contact;
	
	
	
}
