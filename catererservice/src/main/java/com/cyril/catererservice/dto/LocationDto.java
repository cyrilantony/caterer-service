package com.cyril.catererservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class LocationDto {
	
	private String cityName;
	private String street;
	private Integer postal;
	
	
	

}
