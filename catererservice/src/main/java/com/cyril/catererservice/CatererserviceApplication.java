package com.cyril.catererservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatererserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatererserviceApplication.class, args);
	}

}
