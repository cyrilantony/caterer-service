package com.cyril.catererservice.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import org.springframework.messaging.handler.annotation.Payload;

@Documented
@Constraint(validatedBy = AlphabetsOnlyValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AlphabetsOnlyConstraint {
    String message() default "Contains digits and special charaxters";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}