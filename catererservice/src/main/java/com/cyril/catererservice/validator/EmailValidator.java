package com.cyril.catererservice.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.validator.routines.DomainValidator;

public class EmailValidator implements ConstraintValidator<EmailConstraint, String> {

	org.apache.commons.validator.routines.EmailValidator EMAIL_VALIDATOR = new org.apache.commons.validator.routines.EmailValidator(
			false, false, DomainValidator.getInstance(false));

	@Override
	public void initialize(EmailConstraint email) {
	}

	@Override
	public boolean isValid(String emailField, ConstraintValidatorContext cxt) {
		return EMAIL_VALIDATOR.isValid(emailField);
	}

}