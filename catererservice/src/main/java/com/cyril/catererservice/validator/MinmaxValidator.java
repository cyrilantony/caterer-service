package com.cyril.catererservice.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class MinmaxValidator implements ConstraintValidator<MinMaxConstraint, Object> {

	private String minf;
	private String maxf;

	public void initialize(MinMaxConstraint constraintAnnotation) {
		this.minf = constraintAnnotation.minfield();
		this.maxf = constraintAnnotation.maxfield();
	}

	public boolean isValid(Object value, ConstraintValidatorContext context) {

		Integer minV = (Integer) new BeanWrapperImpl(value).getPropertyValue(minf);
		Integer maxV = (Integer) new BeanWrapperImpl(value).getPropertyValue(maxf);

		return minV <= maxV;
	}
}