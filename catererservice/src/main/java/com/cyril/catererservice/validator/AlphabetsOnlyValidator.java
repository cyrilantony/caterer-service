package com.cyril.catererservice.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AlphabetsOnlyValidator implements ConstraintValidator<AlphabetsOnlyConstraint, String> {

	/// only alphabets
	static String REGEX = "^[a-zA-Z]*$";
	static Pattern PATTERN = Pattern.compile(REGEX);

	@Override
	public void initialize(AlphabetsOnlyConstraint contactNumberConstraint) {
	}

	@Override
	public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
		return contactField != null && PATTERN.matcher(contactField).matches();
	}

}