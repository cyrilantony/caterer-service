package com.cyril.catererservice.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.ObjectUtils;

public class ContactNumberValidator implements ConstraintValidator<ContactNumberConstraint, String> {
	
	// valid phone number regex
	String REGEX = "^(\\+\\d{1,2}\\s?)?1?\\-?\\.?\\s?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$";
	Pattern PATTERN= Pattern.compile(REGEX);
	
	boolean optionalNumber = false;
    @Override
    public void initialize(ContactNumberConstraint contactNumberConstraint) {
    	optionalNumber = contactNumberConstraint.optional();
    }

    @Override
    public boolean isValid(String contactField,
      ConstraintValidatorContext cxt) {
    	if(optionalNumber && ObjectUtils.isEmpty(contactField)) {
    		return true;
    	}
        return contactField != null && PATTERN.matcher(contactField).matches();
    }

}