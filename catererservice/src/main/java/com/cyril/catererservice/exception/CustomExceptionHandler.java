package com.cyril.catererservice.exception;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings({ "unchecked", "rawtypes" })
@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<String> handleAllExceptions(Exception ex, WebRequest request) {
		ex.printStackTrace();
		log.error("exception", ex);
		return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<String> handleRecordNotFoundException(RecordNotFoundException ex, WebRequest request) {
		ex.printStackTrace();
		log.error("exception", ex);
		return new ResponseEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
	}

	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		ex.printStackTrace();
		log.error("exception", ex);
		Map<String, Object> map = new HashMap<>();
//		List<FieldError> errors = ex.getFieldErrors();
		
		List<String> errors = ex.getFieldErrors()
                .stream()
                .map(constraintViolation -> {
                    return constraintViolation.getObjectName() +"." + constraintViolation.getField()
                    +"=" + constraintViolation.getRejectedValue()+". Reason for rejection:" + constraintViolation.getDefaultMessage();
                })
                .collect(Collectors.toList());
		Collections.sort(errors);
		map.put("errors", errors);
		return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
	}
}