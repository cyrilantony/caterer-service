package com.cyril.catererservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ServerInternalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3656103208982281502L;

	public ServerInternalException(String exception) {
		super(exception);
	}

}
