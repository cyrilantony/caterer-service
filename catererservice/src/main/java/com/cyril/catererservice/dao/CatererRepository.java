package com.cyril.catererservice.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.cyril.catererservice.dto.CatererDto;

@Repository
public interface CatererRepository extends MongoRepository<CatererDto, String>{

	List<CatererDto> findByName(String name);

	@Query("{'location.cityName' : ?0}") 
	Page<CatererDto> findByCityName(String cityName, PageRequest page);

}
